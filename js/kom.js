var url = "https://dev.vub.zone/sandbox/router.php"
var user;

var komi;
var maxid;



$(document).ready(function () {
    login();
    komponentaGet();
});

$.ajaxSetup({
    xhrFields: {
        withCredentials: true
    }
});

function login(){
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: url,
        data: { "projekt": "p_lnovak", "procedura": "p_login", "email": "lnovak@vub.hr", "lozinka": "123" },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var podaci = jsonBody.data[0].ime + " " + jsonBody.data[0].prezime;
            user=podaci;
            $('#auser').html("User: "+ user);
            console.log("User: "+podaci);
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true,
    });
}

function komponentaGet(){
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: url,
        data: { "projekt": "p_lnovak", "procedura": "p_getkom"},
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var podaci =  jsonBody ;
            komi=podaci.pod;
            maxid=podaci.br;
            $('#k1').html(komi[0].BROJ);
            $('#k2').html(komi[1].BROJ);
            $('#k3').html(komi[2].BROJ);
            $('#k4').html(komi[3].BROJ);
            $('#k5').html(komi[4].BROJ);
            $('#k6').html(komi[5].BROJ);
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true,
    });
}

function komponentaSet(broj, id){
    if(id<1 && id>maxid){
        console.log("Greška: pogrešan id -> "+id);
    }else{
        if(broj==100){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_komp100", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Komponenta id:"+id+" +"+broj);
                    komponentaGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==10){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_komp10", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Komponenta id:"+id+" +"+broj);
                    komponentaGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==1){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_komp1", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Komponenta id:"+id+" +"+broj);
                    komponentaGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==-1){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_komm1", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Komponenta id:"+id+" "+broj);
                    komponentaGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==-10){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_komm10", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Komponenta id:"+id+" "+broj);
                    komponentaGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==-100){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_komm100", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Komponenta id:"+id+" "+broj);
                    komponentaGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else{
            console.log("Greška: pogrešan broj -> "+broj);
        }
    }
}
