var url = "https://dev.vub.zone/sandbox/router.php"
var user;

var tab="Tableti";
var lap="Laptopi";
var rac="Računala";
var kom="Komponente";




//JSON zamjena
var jtab={"br":3,"pod":[{"ID_TAB":1,"NAZIV":"Microsoft Surface New Pro","BROJ":9999},{"ID_TAB":2,"NAZIV":"Lenovo Miix 510 tablet","BROJ":40},{"ID_TAB":3,"NAZIV":"Dell Latitude 7285","BROJ":0}]};
var jlap={"br":6,"pod":[{"ID_LAP":1,"NAZIV":"Lenovo 720s","BROJ":33},{"ID_LAP":2,"NAZIV":"Acer Chromebook 15","BROJ":56},{"ID_LAP":3,"NAZIV":"Lenovo 520","BROJ":33},{"ID_LAP":4,"NAZIV":"ASUS VivoBook Flip TP510UA","BROJ":66},{"ID_LAP":5,"NAZIV":"ASUS VivoBook Flip TP550UA","BROJ":24},{"ID_LAP":6,"NAZIV":"Samsung Chromebook Pro 14","BROJ":100}]};
var jrac={"br":3,"pod":[{"ID_RAC":1,"NAZIV":"HP OMEN 880-120","BROJ":155},{"ID_RAC":2,"NAZIV":"CybertronPC Palladium","BROJ":44},{"ID_RAC":3,"NAZIV":"Lenovo Y920T","BROJ":66}]};
var jkom={"br":6,"pod":[{"ID_KOM":1,"NAZIV":"MSI Z370 PC PRO LGA 1151","BROJ":103},{"ID_KOM":2,"NAZIV":"MSI Z270 GAMING PRO CARBON","BROJ":156},{"ID_KOM":3,"NAZIV":"MSI Z370 GODLIKE GAMING LGA 1151","BROJ":19},{"ID_KOM":4,"NAZIV":"MSI Z370-A PRO LGA 1151","BROJ":45},{"ID_KOM":5,"NAZIV":"Intel NUC7i5BNKP","BROJ":79},{"ID_KOM":6,"NAZIV":"Intel SSD 900P","BROJ":256}]};

//pozivi
$(document).ready(function () {
    login();
    //tableti();
    //laptopi();
    //racunala();
    //komponente();
});

$.ajaxSetup({
    xhrFields: {
        withCredentials: true
    }
});

//login test
function loginG(){
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: url,
        data: { "projekt": "p_common", "procedura": "p_login", "username": 'ggrubisic@gmail.com', "password": 123 },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var podaci = jsonBody.data[0].ime + " " + jsonBody.data[0].prezime;
            user=podaci;
            $('#auser').html("User: "+ user);
            console.log("User: "+user);
            console.log("ggrubisic");
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true,
    });
}


//login u bazu
function login(){
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: url,
        data: { "projekt": "p_lnovak", "procedura": "p_login", "email": "lnovak@vub.hr", "lozinka": "123" },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var podaci = jsonBody.data[0].ime + " " + jsonBody.data[0].prezime;
            user=podaci;
            console.log("User: "+user);
            refreshuser();
            tableti();
            laptopi();
            racunala();
            komponente();
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true,
    });
}

//getter za tablete
function tableti(){
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: url,
        data: { "projekt": "p_lnovak", "procedura": "p_gettab"},
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var podaci = '(' + jsonBody.br + ')' ;
            tab+=podaci;
            $('#tab_br').html(tab);

            //debug log
            //console.log(JSON.stringify((jsonBody),null,2));
            //console.log(podaci);
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true,
    });
}

//getter za laptope
function laptopi(){
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: url,
        data: { "projekt": "p_lnovak", "procedura": "p_getlap"},
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var podaci = '(' + jsonBody.br + ')' ;       
            lap+=podaci;
            $('#lap_br').html(lap);
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true,
    });
}

//getter za racunala
function racunala(){
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: url,
        data: { "projekt": "p_lnovak", "procedura": "p_getrac"},
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var podaci = '(' + jsonBody.br + ')' ;
            rac+=podaci;
            $('#rac_br').html(rac);

        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true,
    });
}

//getter za komponente
function komponente(){
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: url,
        data: { "projekt": "p_lnovak", "procedura": "p_getkom"},
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var podaci = '(' + jsonBody.br + ')' ;
            kom+=podaci;
            $('#kom_br').html(kom);
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true,
    });
}

//ispis na stranicu
function refreshpro(){
    $('#tab_br').html(tab);
    $('#lap_br').html(lap);
    $('#rac_br').html(rac);
    $('#kom_br').html(kom);
    console.log("refreshpro");
}

function refreshuser(){
    $('#auser').html("User: "+ user);
    console.log("refreshuser");
}

