create or replace NONEDITIONABLE PACKAGE BODY ROUTER AS

e_iznimka EXCEPTION;
procedure p_getzupanije(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
  l_obj JSON_OBJECT_T;
  l_zupanija varchar2(8000);
  l_zupanije json_array_t :=JSON_ARRAY_T('[]');
  l_count number;
  l_string varchar2(4000);
  l_search varchar2(30);
  CURSOR c_zupanije IS
    SELECT json_object('ID' VALUE ID, 
                        'ZUPANIJA' VALUE ZUPANIJA)
    FROM
        zupanije
    WHERE
         upper(ZUPANIJA) LIKE ('%' || upper(l_search) || '%');
    BEGIN
    l_obj := JSON_OBJECT_T(in_json);
    l_string := l_obj.TO_STRING;
    
    SELECT
        JSON_VALUE(l_string, '$.search' RETURNING VARCHAR2)
    INTO
        l_search
    FROM DUAL;
    
    OPEN c_zupanije;
    LOOP
        fetch c_zupanije into l_zupanija;
        exit when c_zupanije%notfound;
        l_zupanije.append(JSON_OBJECT_T(l_zupanija));
    END LOOP;
    CLOSE c_zupanije;
    
    select 
        count(1)
    into 
        l_count
    from
        zupanije;
        
    l_obj.put('count',l_count);
    l_obj.put('data',l_zupanije);
    out_json := l_obj;
    
  END p_getzupanije;

-----------------------------------------------------------------

    procedure p_login(in_json in JSON_OBJECT_T, 
                      out_json out JSON_OBJECT_T) AS
       l_obj      JSON_OBJECT_T := JSON_OBJECT_T();
       l_input    varchar2(4000);
       l_record   varchar2(4000);
       j_email    korisnici.email%type;
       j_lozinka korisnici.lozinka%type;
       b_lozinka korisnici.lozinka%type;
       b_id       korisnici.id%type;
       l_out      json_array_t := JSON_ARRAY_T('[]');     
       l_sqlcode varchar2(10);
    
    BEGIN
       l_obj.put('h_message', ''); 
       l_obj.put('h_errcod', '');
       
       l_input := in_json.TO_STRING;
       
       SELECT 
          JSON_VALUE(l_input, '$.email' RETURNING VARCHAR2), 
          JSON_VALUE(l_input, '$.lozinka' RETURNING VARCHAR2) 
       INTO
          j_email,
          j_lozinka
       FROM DUAL;
    
       if (j_email is null) then
          l_obj.put('h_message', 'Molimo unesite email i lozinku'); 
          l_obj.put('h_errcod', 101); 
          raise e_iznimka;
       else
          begin
             select 
                id
             into
                b_id
             from
                korisnici
             where 
                email = j_email and 
                lozinka = nvl(j_lozinka, ' ');
          exception
             when no_data_found then
                l_obj.put('h_message', 'Nepoznat e-mail ili lozinka'); 
                l_obj.put('h_errcod', 102); 
                raise e_iznimka;
             when others then
                raise;
          end; 
         
          SELECT 
             json_object('ID' VALUE ID, 
                         'ime' VALUE ime,
                         'prezime' VALUE prezime) 
          INTO 
             l_record 
          FROM 
             korisnici
          where
             id = b_id;
       end if;
      
       l_out.append(JSON_OBJECT_T(l_record));
       l_obj.put('data', l_out);
       out_json := l_obj;
         
    EXCEPTION
      WHEN e_iznimka THEN
        out_json := l_obj;
    END p_login;
-----------------------------------------------------------------------------------

  procedure p_main(p_in in varchar2, p_out out varchar2) AS
    l_obj JSON_OBJECT_T;
    l_string varchar2(4000);
    l_procedura varchar2(40);
  BEGIN
    l_obj := JSON_OBJECT_T(p_in);
    l_string := l_obj.TO_STRING;

    SELECT
        JSON_VALUE(l_string, '$.procedura' RETURNING VARCHAR2)
    INTO
        l_procedura
    FROM DUAL;

    CASE l_procedura
    WHEN 'p_login' THEN
        p_login(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_zupanije' THEN
        p_getzupanije(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_insertkorisnici' THEN
        insert_korisnici.p_insertkorisnici(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_printkorisnici' THEN
        print_korisnici.p_printkorisnici(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_editkorisnici' THEN
        edit_korisnici.p_editkorisnici(JSON_OBJECT_T(p_in), l_obj);
    ELSE
        l_obj.put('h.message', ' Nepoznata metoda' || l_procedura);
        l_obj.put('h_errcod', 997);
    END CASE;
    p_out := l_obj.TO_STRING;
  END p_main;

END ROUTER;