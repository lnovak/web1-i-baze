var url = "https://dev.vub.zone/sandbox/router.php"
var user;

var tabi;
var maxid;

var jtab={"br":3,"pod":[{"ID_TAB":1,"NAZIV":"Microsoft Surface New Pro","BROJ":9999},{"ID_TAB":2,"NAZIV":"Lenovo Miix 510 tablet","BROJ":40},{"ID_TAB":3,"NAZIV":"Dell Latitude 7285","BROJ":0}]};
//tabi=jtab.pod;
//maxid=jtab.br;

$(document).ready(function () {
    login();
    tabletGet();
});

$.ajaxSetup({
    xhrFields: {
        withCredentials: true
    }
});

function login(){
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: url,
        data: { "projekt": "p_lnovak", "procedura": "p_login", "email": "lnovak@vub.hr", "lozinka": "123" },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var podaci = jsonBody.data[0].ime + " " + jsonBody.data[0].prezime;
            user=podaci;
            $('#auser').html("User: "+ user);
            console.log("User: "+podaci);
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true,
    });
}

function tabletGet(){
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: url,
        data: { "projekt": "p_lnovak", "procedura": "p_gettab"},
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var podaci =  jsonBody ;
            tabi=podaci.pod;
            maxid=podaci.br;
            $('#t1').html(tabi[0].BROJ);
            $('#t2').html(tabi[1].BROJ);
            $('#t3').html(tabi[2].BROJ);
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true,
    });
}

function tabletSet(broj, id){
    if(id<1 && id>maxid){
        console.log("Greška: pogrešan id -> "+id);
    }else{
        if(broj==100){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_tabp100", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Tablet id:"+id+" +"+broj);
                    tabletGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==10){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_tabp10", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Tablet id:"+id+" +"+broj);
                    tabletGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==1){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_tabp1", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Tablet id:"+id+" +"+broj);
                    tabletGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==-1){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_tabm1", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Tablet id:"+id+" "+broj);
                    tabletGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==-10){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_tabm10", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Tablet id:"+id+" "+broj);
                    tabletGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==-100){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_tabm100", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Tablet id:"+id+" "+broj);
                    tabletGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else{
            console.log("Greška: pogrešan broj -> "+broj);
        }
    }
}
