var url = "https://dev.vub.zone/sandbox/router.php"
var user;

var raci;
var maxid;



$(document).ready(function () {
    login();
    racunaloGet();
});

$.ajaxSetup({
    xhrFields: {
        withCredentials: true
    }
});

function login(){
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: url,
        data: { "projekt": "p_lnovak", "procedura": "p_login", "email": "lnovak@vub.hr", "lozinka": "123" },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var podaci = jsonBody.data[0].ime + " " + jsonBody.data[0].prezime;
            user=podaci;
            $('#auser').html("User: "+ user);
            console.log("User: "+podaci);
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true,
    });
}

function racunaloGet(){
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: url,
        data: { "projekt": "p_lnovak", "procedura": "p_getrac"},
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var podaci =  jsonBody ;
            raci=podaci.pod;
            maxid=podaci.br;
            $('#r1').html(raci[0].BROJ);
            $('#r2').html(raci[1].BROJ);
            $('#r3').html(raci[2].BROJ);
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true,
    });
}

function racunaloSet(broj, id){
    if(id<1 && id>maxid){
        console.log("Greška: pogrešan id -> "+id);
    }else{
        if(broj==100){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_racp100", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Racunalo id:"+id+" +"+broj);
                    racunaloGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==10){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_racp10", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Racunalo id:"+id+" +"+broj);
                    racunaloGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==1){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_racp1", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Racunalo id:"+id+" +"+broj);
                    racunaloGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==-1){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_racm1", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Racunalo id:"+id+" "+broj);
                    racunaloGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==-10){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_racm10", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Racunalo id:"+id+" "+broj);
                    racunaloGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==-100){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_racm100", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Racunalo id:"+id+" "+broj);
                    racunaloGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else{
            console.log("Greška: pogrešan broj -> "+broj);
        }
    }
}
