var url = "https://dev.vub.zone/sandbox/router.php"
var user;

var lapi;
var maxid;



$(document).ready(function () {
    login();
    laptopGet();
});

$.ajaxSetup({
    xhrFields: {
        withCredentials: true
    }
});

function login(){
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: url,
        data: { "projekt": "p_lnovak", "procedura": "p_login", "email": "lnovak@vub.hr", "lozinka": "123" },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var podaci = jsonBody.data[0].ime + " " + jsonBody.data[0].prezime;
            user=podaci;
            $('#auser').html("User: "+ user);
            console.log("User: "+podaci);
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true,
    });
}

function laptopGet(){
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: url,
        data: { "projekt": "p_lnovak", "procedura": "p_getlap"},
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var podaci =  jsonBody ;
            lapi=podaci.pod;
            maxid=podaci.br;
            $('#l1').html(lapi[0].BROJ);
            $('#l2').html(lapi[1].BROJ);
            $('#l3').html(lapi[2].BROJ);
            $('#l4').html(lapi[3].BROJ);
            $('#l5').html(lapi[4].BROJ);
            $('#l6').html(lapi[5].BROJ);
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true,
    });
}

function laptopSet(broj, id){
    if(id<1 && id>maxid){
        console.log("Greška: pogrešan id -> "+id);
    }else{
        if(broj==100){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_lapp100", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Laptop id:"+id+" +"+broj);
                    laptopGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==10){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_lapp10", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Laptop id:"+id+" +"+broj);
                    laptopGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==1){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_lapp1", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Laptop id:"+id+" +"+broj);
                    laptopGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==-1){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_lapm1", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Laptop id:"+id+" "+broj);
                    laptopGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==-10){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_lapm10", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Laptop id:"+id+" "+broj);
                    laptopGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else if(broj==-100){
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: url,
                data: { "projekt": "p_lnovak", "procedura": "p_lapm100", "broj": id},
                success: function (data) {
                    var jsonBody = JSON.parse(data);
                    var podaci =  jsonBody ;
                    console.log(podaci);
                    console.log("Laptop id:"+id+" "+broj);
                    laptopGet();
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                },
                async: true,
            });
        }else{
            console.log("Greška: pogrešan broj -> "+broj);
        }
    }
}
